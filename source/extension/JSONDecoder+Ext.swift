//
//  JSONDecoder+Ext.swift
//  posa
//
//  Created by JaxunC on 2018/9/11.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

enum JSONDecoderError: Error{
    case convertDictToData
}

extension JSONDecoder{
    func decode<T>(_ type: T.Type, from dict: [String: Any]) throws -> T where T : Decodable{
        guard let data = try? JSONSerialization.data(withJSONObject: dict, options: []) else {
            throw JSONDecoderError.convertDictToData
        }
        do {
            return try decode(type, from: data)
        } catch let error {
            print(error)
            throw error
        }
    }
    
    func decodeToArray<T>(_ type: T.Type, from dicts: [Dictionary<String, Any>]) throws -> [T] where T : Decodable {
        return dicts.compactMap{ dict in
            try? decode(type, from: dict)
        }
    }
}
