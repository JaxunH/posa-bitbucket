//
// Created by STARLUX on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SwiftTheme

struct Themes {
  static let lightTextColor = UIColor.white
  static let darkTextColor = UIColor.lightGray
  static let starluxGold = UIColor(hex: 0x827553)
  static let starluxBlack = UIColor(hex: 0x3F464B)
  static let starluxLight = UIColor(hex: 0xF2F1ED)
  static let starluxGray = UIColor(hex: 0xC0B7A5)
  static let starluxBg = UIColor(hex: 0xF2F2F2)
  static let warningRed = UIColor(hex: 0xB33634)
  static let badgeRed = UIColor(hex: 0xBA2228)
  static let blurBg = UIColor.darkGray.withAlphaComponent(0.6)

  static let starluxGoldImg = UIImage(color: starluxGold)!
  static let starluxLightImg = UIImage(color: starluxLight)!
  static let starluxGrayImg = UIImage(color: starluxGray)!
  static let whiteImg = UIImage(color: .white)!

  static let backgroundColor = ThemeColorPicker.pickerWithColors(["#fff", "#888", "#000"])
  static let errorBackgroundColor = ThemeColorPicker.pickerWithColors(["#f00", "#f00", "#f00"])
  static let backgroundColorCell = ThemeColorPicker.pickerWithColors(["#fff", "#888", "#000"])
  static let themedTextColorLight = ThemeColorPicker.pickerWithColors(["#000", "#000", "#fff"])
  static let themedTextColorDark = ThemeColorPicker.pickerWithColors(["#fff", "#fff", "#000"])
  static let numberOfThemes = 3
  static let separatorColor = ThemeColorPicker.pickerWithColors(["#000", "#000", "#fff"])

  static let coverBackgroundColor = UIColor.orange

  static let font10 = UIFont.systemFont(ofSize: 10)
  static let font12 = UIFont.systemFont(ofSize: 12)
  static let font12Bold = UIFont.boldSystemFont(ofSize: 12)
  static let font14 = UIFont.systemFont(ofSize: 14)
  static let font15 = UIFont.systemFont(ofSize: 15)
  static let font15Bold = UIFont.boldSystemFont(ofSize: 15)
  static let font17 = UIFont.systemFont(ofSize: 17)
  static let font11 = UIFont.systemFont(ofSize: 11)
  static let font17Bold = UIFont.boldSystemFont(ofSize: 17)
  static let font20 = UIFont.systemFont(ofSize: 20)
  static let font20Bold = UIFont.boldSystemFont(ofSize: 20)
  static let font30 = UIFont.systemFont(ofSize: 30)
  static let font30Bold = UIFont.boldSystemFont(ofSize: 30)
  static let font40 = UIFont.systemFont(ofSize: 40)
  static let font50 = UIFont.boldSystemFont(ofSize: 50)
  static let font80 = UIFont.boldSystemFont(ofSize: 80)
  static let font100 = UIFont.boldSystemFont(ofSize: 100)

  //AppleSDGothicNeo-Thin
  static let font30NewSec = UIFont(name: "AppleSDGothicNeo-Thin", size: 30)  // for current time display.
  static let font25NewSec = UIFont(name: "AppleSDGothicNeo-Thin", size: 25)  // for current time display.
  static let font20NewSec = UIFont(name: "AppleSDGothicNeo-Thin", size: 20)  // for current time display.
  static let font10FuturaM = UIFont(name: "Futura-Medium", size: 10)
  static let font12FutureM = UIFont(name: "Futura-Medium", size: 12)
  static let font15FutureM = UIFont(name: "Futura-Medium", size: 15)
  static let font17FutureM = UIFont(name: "Futura-Medium", size: 17)
  static let font20FutureM = UIFont(name: "Futura-Medium", size: 20)
  static let font30FutureM = UIFont(name: "Futura-Medium", size: 30)
  static let font50FutureM = UIFont(name: "Futura-Medium", size: 50)
  static let font80FutureM = UIFont(name: "Futura-Medium", size: 80)

  static let font100FutureM = UIFont(name: "Futura-Medium", size: 100)
}