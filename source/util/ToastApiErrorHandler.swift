//
// Created by jaxun on 12/30/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import TTGSnackbar
import RxSwift
import UIKit
import SwiftTheme
import PKHUD
import Alamofire

enum ApiError: Swift.Error, Equatable {
    case resourceNotFoundError
    case authenticationError
    case serviceError
    case internalServerError
    case networkError
    case httpStatusError
    case programBugError(AFError)
    
    static func convertError(_ error: Swift.Error) -> ApiError {
        guard let afError = (error as? AFError) else {
            return serviceError//networkError todo: seem 104 server connect error will return from here.
        }
        guard let responseCode = afError.responseCode else {
            return programBugError(afError)
        }
        if (responseCode == 400) {
            return serviceError
        } else if (responseCode == 401 || responseCode == 403) {
            return authenticationError
        } else if (responseCode == 404) {
            return resourceNotFoundError
        } else if (responseCode >= 500 && responseCode < 600) {
            return internalServerError
        } else {
            return httpStatusError
        }
    }
    
    static func ==(lhs: ApiError, rhs: ApiError) -> Bool {
        switch (lhs, rhs) {
        case (.programBugError, .programBugError):
            return true
        case (.serviceError, .serviceError):
            return true
        case (.resourceNotFoundError, .resourceNotFoundError):
            return true
        case (.authenticationError, .authenticationError):
            return true
        case (.internalServerError, .internalServerError):
            return true
        case (.networkError, .networkError):
            return true
        case (.httpStatusError, .httpStatusError):
            return true
        default:
            return false
        }
    }
}


private func toast(_ msg:String) {
    
    // api error handler may be called before key window initialization, so
    // we have to do one frame delay and executed in main thread to prevent crash
    _ = Observable.just(())
        .delay(0.03, scheduler: MainScheduler.instance)
        .subscribe(onNext: {
            let bar:TTGSnackbar = TTGSnackbar.init(message: msg, duration: .short)
            //bar.theme_backgroundColor = Themes.errorBackgroundColor
            bar.backgroundColor = Themes.warningRed
            bar.show()
        })
}

func toastApiErrorHandler(apiError: ApiError) {
    
    HUD.hide()
    
    switch apiError {
    case .programBugError(_):
        #if DEBUG
        //fatalError("calling api signature) error: \(afError)")
        #else
        toast("*** " + NSLocalizedString("retrofit_service_error", comment: ""))
        #endif
    case .serviceError:
        toast(NSLocalizedString("retrofit_service_error", comment: ""));
    case .authenticationError:
        toast(NSLocalizedString("retrofit_authentication_error", comment: ""))
    case .httpStatusError:
        toast(NSLocalizedString("retrofit_http_status_error", comment: ""))
    case .internalServerError:
        toast(NSLocalizedString("retrofit_internal_server_error", comment: ""))
    case .networkError:
        toast(NSLocalizedString("retrofit_network_error", comment: ""))
    case .resourceNotFoundError:
        toast(NSLocalizedString("retrofit_resource_not_found_error", comment: ""))
    }
}
