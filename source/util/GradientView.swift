//
// Created by STARLUX on 1/6/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class GradientView: UIView {

  @IBInspectable var startColor: UIColor = .black
  @IBInspectable var endColor:   UIColor = UIColor(hex: 0xaaaaaa)

  @IBInspectable var startLocation: Double = 0.05
  @IBInspectable var endLocation:   Double = 0.95

  @IBInspectable var horizontalMode: Bool = true
  @IBInspectable var diagonalMode: Bool = false

  override class var layerClass: AnyClass { return CAGradientLayer.self }
  var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }

  init(startColor: UIColor, endColor:UIColor, horizontalMode:Bool) {
    self.startColor = startColor
    self.endColor = endColor

    self.horizontalMode = horizontalMode
    super.init(frame: CGRect.zero)
  }

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }


  override func layoutSubviews() {
    super.layoutSubviews()
    if horizontalMode {
      gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
      gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
    } else {
      gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
      gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
    }
    gradientLayer.locations = [startLocation as NSNumber,  endLocation  as NSNumber]
    gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
  }
}
