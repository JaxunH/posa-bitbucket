//
//  CalendarVMP.swift
//  posa
//
//  Created by JaxunC on 2018/9/17.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

protocol CalendarVMP {
    var mainCalenderService: MainCalenderServiceManager { get }
    var monthlyPunchTimeDmArr: [MonthlyPunchTimeDm] { get set }
    var todayRecords: [String] { get set }
    var abnormalRecordList: [BaseAbnormalDm] { get set }
    var monthlyMealOrderRecordList: [BaseMealOrderDm] { get set }
    var abnormalDayList: [String] { get set }
    func showStatus(date: Date, first: (Bool) -> Void, second: (Bool) -> Void) -> (first: String, second: String)
    func updateWorkTime(_ selectDay: String) -> String
    
    //labelTexts
    var punchTimeListLabel: String { get set }
    var orderMealLabel: String { get set }
    var breakTimeDesc: String { get set }
    var breakTimeLabel: String { get set }
    var orderMealDesc: String { get set }
    var workTimeDesc: String { get set }
    var workTimeLabel: String { get set }
    var forgotSecondButtonTitle: String { get set }
    var leaveSecondButtonTitle: String { get set }
    var detectText: String { get set }
    var forgotButtonTitle: String { get set }
    var leaveButtonTitle: String { get set }
    
    var fetchYearMonth: PublishSubject<(String, String)> { get }
    var cardData: Observable<String> { get }
    var mealOrder: Observable<String> { get }
    var cardDataMatch: Observable<Void> { get }
    
}
