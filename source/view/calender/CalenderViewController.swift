//
// Created by Jaxun on 2018/5/15.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import SnapKit
import FSCalendar
import PKHUD
import Datez

class CalenderViewController: UIViewController{
    //
    var vm: CalendarVMP = CalendarViewModel()
    
    //MARK:- part of top views
    fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
    fileprivate lazy var preMonthBtn:UIButton = {
        $0.backgroundColor = Themes.starluxGold
        $0.setBackgroundImage(UIImage(named: "cal_left_white"), for: .normal)
        $0.setTitleColor(.white, for: UIControlState.normal)
        $0.layer.shadowOffset = CGSize(width: 0, height: 5)
        $0.addTarget(self, action: #selector(preClick), for: UIControlEvents.touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate lazy var nextMonthBtn:UIButton = {
        $0.backgroundColor = Themes.starluxGold
        $0.setBackgroundImage(UIImage(named: "cal_right_white"), for: .normal)
        $0.setTitleColor(.white, for: UIControlState.normal)
        $0.layer.shadowOffset = CGSize(width: 0, height: 5)
        $0.addTarget(self, action: #selector(nextClick), for: UIControlEvents.touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate let topCalenderBgView = UIView(frame: .zero)
    fileprivate let calender = FSCalendar(frame: .zero)
    
    //MARK:- part of bottom views
    fileprivate let scrollView = UIScrollView(frame: .zero) // put items in it.
    fileprivate let scrollContentView = UIView(frame: .zero)
    
    fileprivate let selectDayLabel = UILabel(frame: .zero)
    fileprivate let workTimeLabel = UILabel(frame: .zero)
    fileprivate let workTimeDesc = UILabel(frame: .zero)
    fileprivate let breakTimeLabel = UILabel(frame: .zero)
    fileprivate let breakTimeDesc = UILabel(frame: .zero)
    fileprivate let punchTimeListLabel = UILabel(frame: .zero)
    fileprivate let orderMealLabel = UILabel(frame: .zero)
    fileprivate let orderMealDesc = UILabel(frame: .zero)
    
    fileprivate let grayLine = UIView(frame: .zero)
    fileprivate let separatorLine = UIView(frame: .zero)
    fileprivate let separatorLine2 = UIView(frame: .zero)
    fileprivate let separatorLine3 = UIView(frame: .zero)
    
    fileprivate lazy var abnormalLabel: UILabel = {
        $0.text = vm.detectText
        $0.textColor = Themes.warningRed
        return $0
    }(UILabel(frame: .zero))
    
    fileprivate lazy var leaveButton: UIButton = {
        $0.setTitle(vm.leaveButtonTitle, for: .normal)
        $0.setTitleColor(Themes.starluxGold, for: .normal)
        $0.backgroundColor = .white
        $0.layer.shadowRadius = 3
        $0.layer.shadowOpacity = 0.3
        $0.layer.cornerRadius = 15
        $0.layer.shadowOffset = CGSize(width: 0, height: 2)
        $0.addTarget(self, action: #selector(clickToApplyLeave), for: .touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate lazy var centerClearSeparator: UIView = {
        $0.backgroundColor = .clear
        return $0
    }(UIView(frame: .zero))
    
    fileprivate lazy var forgotButton: UIButton = {
        $0.setTitle(vm.forgotButtonTitle, for: .normal)
        $0.setTitleColor(Themes.starluxGold, for: .normal)
        $0.backgroundColor = .white
        $0.layer.shadowRadius = 3
        $0.layer.shadowOpacity = 0.3
        $0.layer.cornerRadius = 15
        $0.layer.shadowOffset = CGSize(width: 0, height: 2)
        $0.addTarget(self, action: #selector(clickToApplyForgot), for: .touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate lazy var abnormalSecondLabel: UILabel = {
        $0.text = vm.detectText // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
        $0.textColor = Themes.warningRed
        return $0
    }(UILabel(frame: .zero))
    
    fileprivate lazy var leaveSecondButton: UIButton = {
        $0.setTitle(vm.leaveSecondButtonTitle, for: .normal)
        $0.setTitleColor(Themes.starluxGold, for: .normal)
        $0.backgroundColor = .white
        $0.layer.shadowRadius = 3
        $0.layer.shadowOpacity = 0.3
        $0.layer.cornerRadius = 15
        $0.layer.shadowOffset = CGSize(width: 0, height: 2)
        $0.addTarget(self, action: #selector(clickToApplyLeave), for: .touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate lazy var forgotSecondButton: UIButton = {
        $0.setTitle(vm.forgotSecondButtonTitle, for: .normal)
        $0.setTitleColor(Themes.starluxGold, for: .normal)
        $0.backgroundColor = .white
        $0.layer.shadowRadius = 3
        $0.layer.shadowOpacity = 0.3
        $0.layer.cornerRadius = 15
        $0.layer.shadowOffset = CGSize(width: 0, height: 2)
        $0.addTarget(self, action: #selector(clickToApplyForgot), for: .touchUpInside)
        return $0
    }(UIButton(type: .system))
    
    fileprivate lazy var todayRecordsTableView: UITableView = {
        $0.delegate = self
        $0.dataSource = self
        $0.separatorStyle = .none
        $0.backgroundColor = Themes.starluxBg
        $0.allowsSelection = false
        $0.flashScrollIndicators()
        $0.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        return $0
    }(UITableView(frame: .zero, style: .plain))
    
    //MARK:- Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviLogo()
        
        let fDate = Utils.getCalendarSelectDateFormat(Date())
        selectDayLabel.text = fDate.uppercased()
        
        let currentMonth = Utils.getMonth(Date())
        let currentYear = Utils.getYear(Date())
        
        calenderUI()
        topContentUI()
        middleContentUI()
        
        //abnormalUI
        scrollContentView.addSubview(abnormalLabel)
        scrollContentView.addSubview(leaveButton)
        scrollContentView.addSubview(centerClearSeparator)
        scrollContentView.addSubview(abnormalSecondLabel)
        scrollContentView.addSubview(forgotButton)
        
        //abnormalSecondUI
        scrollContentView.addSubview(leaveSecondButton)
        scrollContentView.addSubview(forgotSecondButton)
        scrollContentView.addSubview(todayRecordsTableView)
        
        initLayout()
        
        vm.cardData.subscribe(onNext: { [weak self] text in
            HUD.hide(afterDelay: 0.2)
            self?.updateWorkTimeDesc(text: text)
        }).disposed(by: disposeBag)
        
        vm.mealOrder
            .bind(to: orderMealDesc.rx.text).disposed(by: disposeBag)
        
        vm.cardDataMatch.subscribe(onNext: { [weak self] _ in
            self?.calender.reloadData()
            self?.showTodayAbnormalStatus(date: Date())
        }).disposed(by: disposeBag)
        
        fetchApi(choseYear: currentYear, choseMonth: currentMonth)
    }
    
    override func viewWillAppear(_ animated:Bool) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
    }
}

extension CalenderViewController{
    
    fileprivate func showTodayAbnormalStatus(date:Date) {
        // hide abnormal first, update current day abnormal
        let (firstText, secondText) = vm.showStatus(date: date, first: { (boolVal) in
            abnormalLabel.isHidden = boolVal
            forgotButton.isHidden = boolVal
            leaveButton.isHidden = boolVal
        }) { (boolVal) in
            abnormalSecondLabel.isHidden = boolVal
            forgotSecondButton.isHidden = boolVal
            leaveSecondButton.isHidden = boolVal
        }
        
        abnormalLabel.text = firstText
        abnormalSecondLabel.text = secondText
    }
}


extension CalenderViewController {
    //Fetchs
    fileprivate func fetchApi(choseYear:String, choseMonth:String) {
        HUD.show(.progress)
        vm.fetchYearMonth.onNext( (choseYear, choseMonth) )
    }
}

// MARK: Tap calender
extension CalenderViewController {
    @objc func preClick() {
        let preMonth = calender.currentPage - 1
        //print(preMonth)
        calender.setCurrentPage(preMonth, animated: true)
        
        let choseMonth = Utils.getMonth(preMonth)
        let currentYear = Utils.getYear(Date())
        fetchApi(choseYear: currentYear, choseMonth: choseMonth)
    }
    
    @objc func nextClick() {
        let nextMonth = calender.date(byAddingMonths: 1, to: calender.currentPage)
        calender.setCurrentPage(nextMonth, animated: true)
        //print(nextMonth)
        
        let choseMonth = Utils.getMonth(nextMonth)
        let currentYear = Utils.getYear(Date())
        fetchApi(choseYear: currentYear, choseMonth: choseMonth)
    }
    
    @objc func clickToApplyForgot() {
        let controller = FormApplyEntry(type: .forgot)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func clickToApplyLeave() {
        let controller = FormApplyEntry(type: .calendar)
        navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK:
extension CalenderViewController: FSCalendarDataSource, FSCalendarDelegate {
    
    func calendarCurrentMonthDidChange(_ calendar:FSCalendar) {
        let choseMonth = Utils.getMonth(calendar.currentPage)
        let currentYear = Utils.getYear(Date())
        
        fetchApi(choseYear: currentYear, choseMonth: choseMonth)
    }
    
    func calendar(_ calendar:FSCalendar, numberOfEventsFor date:Date) -> Int {
        //print("day-----   \(date)")
        let dayFormat = Utils.getFullDateFormat(date)
        //print("dayFormat-----   \(dayFormat)")
        if vm.abnormalDayList.contains(dayFormat) {
            return 1
        }
        //print("abnormalDayList\(abnormalDayList)")
        for (_, dto) in vm.abnormalRecordList.enumerated() {
            let baseAbnormalDto = dto as BaseAbnormalDm
            let eventDay = Utils.getFullDateFormat(date)
            //print(eventDay)
            if eventDay == baseAbnormalDto.date {
                //print(eventDay)
                return 1
            }
        }
        return 0
    }
    
    func calendar(_ calendar:FSCalendar, boundingRectWillChange bounds:CGRect, animated:Bool) {
        calender.snp.updateConstraints { (make) in
            make.height.equalTo(bounds.height)
        }
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar:FSCalendar, didSelect date:Date, at monthPosition:FSCalendarMonthPosition) {
        //print(date)
        
        let fDate = Utils.getCalendarSelectDateFormat(date)
        selectDayLabel.text = fDate.uppercased()
        
        let selectDay = date.gregorian + 8.hour
        //print(selectDay.date)
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(selectDay.date, animated: true)
            // update month api
        }
        
        showTodayAbnormalStatus(date: selectDay.date)
        
        let selectDayStr = Utils.getFullDateFormat(selectDay.date)
        vm.todayRecords.removeAll()
        
        //---show today meal order---//
        let aSelection = vm.monthlyMealOrderRecordList.first(where: { selectDayStr == $0.date})?.selection
        orderMealDesc.text = aSelection ?? ""
        
        //---show today work time---//
        let text = vm.updateWorkTime(selectDayStr)
        updateWorkTimeDesc(text: text)
    }
    
    fileprivate func updateWorkTimeDesc(text: String) {
        todayRecordsTableView.flashScrollIndicators() // show indicators when need it.
        workTimeDesc.text = text
        todayRecordsTableView.reloadData()
        updateTodayRecordsLayout()
    }
    
    fileprivate func withOutSec(_ timeWithSec:String) -> String {
        return String(timeWithSec.prefix(5))
    }
    
    // hide or show "Details"
    fileprivate func updateTodayRecordsLayout() {
        punchTimeListLabel.isHidden = vm.todayRecords.count > 0 ? false : true
        todayRecordsTableView.snp.updateConstraints { make in
            make.height.equalTo(120)
            if vm.todayRecords.count == 0 {
                make.height.equalTo(0)
            }
        }
    }
}

extension CalenderViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        return 30
    }
    
    public func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return vm.todayRecords.count
    }
    
    public func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.backgroundColor = Themes.starluxBg
        cell.textLabel?.text = vm.todayRecords[indexPath.row]
        cell.textLabel?.textColor = Themes.starluxBlack
        cell.textLabel?.textAlignment = .center
        return cell
    }
}

//MARK:- LayoutSetting
extension CalenderViewController {
    
    fileprivate func initLayout() {
        logoImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }
        topCalenderBgView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(0)
            make.height.greaterThanOrEqualTo(40)
        }
        preMonthBtn.snp.makeConstraints { make in
            make.top.equalTo(3)
            make.leading.equalTo(20)
            make.height.greaterThanOrEqualTo(24)
            make.size.equalTo(CGSize(width: 36, height: 36))
        }
        nextMonthBtn.snp.makeConstraints { make in
            make.top.equalTo(3)
            make.trailing.equalTo(-15)
            make.height.greaterThanOrEqualTo(24)
            make.size.equalTo(CGSize(width: 36, height: 36))
        }
        calender.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(0)
            make.height.equalTo(300)
        }
        
        //--------- insert in scroll view ----------//
        selectDayLabel.snp.makeConstraints { make in
            make.top.equalTo(scrollContentView).offset(20)
            make.centerX.equalTo(view)
        }
        grayLine.snp.makeConstraints { make in
            make.bottom.equalTo(calender)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(2)
        }
        separatorLine.snp.makeConstraints { make in
            make.top.equalTo(selectDayLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
        }
        workTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(separatorLine.snp.bottom).offset(20)
            make.leading.equalTo(scrollContentView).inset(50)
        }
        workTimeDesc.snp.makeConstraints { make in
            make.centerY.equalTo(workTimeLabel)
            make.centerX.equalTo(todayRecordsTableView)
        }
        breakTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(workTimeDesc.snp.bottom).offset(20)
            make.leading.equalTo(scrollContentView).inset(50)
        }
        breakTimeDesc.snp.makeConstraints { make in
            make.centerY.equalTo(breakTimeLabel)
            make.centerX.equalTo(todayRecordsTableView)
        }
        separatorLine2.snp.makeConstraints { make in
            make.top.equalTo(breakTimeDesc.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
        }
        orderMealLabel.snp.makeConstraints { make in
            make.top.equalTo(separatorLine2.snp.bottom).offset(20)
            make.centerX.equalTo(breakTimeLabel)
        }
        orderMealDesc.snp.makeConstraints { make in
            make.centerY.equalTo(orderMealLabel)
            make.centerX.equalTo(todayRecordsTableView)
        }
        separatorLine3.snp.makeConstraints { make in
            make.top.equalTo(orderMealLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
        }
        punchTimeListLabel.snp.makeConstraints { make in
            make.top.equalTo(separatorLine3.snp.bottom).offset(20)
            make.centerX.equalTo(breakTimeLabel)
            /*if todayRecords.count == 0 {
             make.height.equalTo(0)
             }*/
        }
        todayRecordsTableView.snp.makeConstraints { make in
            make.top.equalTo(punchTimeListLabel).offset(-3)
            make.leading.equalTo(punchTimeListLabel.snp.trailing).offset(20)
            make.trailing.equalTo(-20)
            make.height.equalTo(120)
            /*if todayRecords.count == 0 {
             make.height.equalTo(0)
             }*/
        }
        abnormalLabel.snp.makeConstraints { make in
            make.top.equalTo(todayRecordsTableView.snp.bottom).offset(20)
            make.centerX.equalTo(view)
            //make.leading.equalTo(workTimeLabel)
        }
        leaveButton.snp.makeConstraints { make in
            make.top.equalTo(abnormalLabel.snp.bottom).offset(20)
            make.centerX.equalTo(abnormalLabel)
            make.size.equalTo(CGSize(width: 240, height: 40))
        }
        
        // base standard separator
        centerClearSeparator.snp.makeConstraints { make in
            make.top.equalTo(abnormalLabel.snp.bottom)
            make.centerX.equalTo(view)
            make.height.equalTo(1)
        }
        
        forgotButton.snp.makeConstraints { make in
            
            make.top.equalTo(leaveButton.snp.bottom).offset(20)
            make.centerX.equalTo(abnormalLabel)
            make.size.equalTo(CGSize(width: 240, height: 40))
        }
        
        abnormalSecondLabel.snp.makeConstraints { make in
            make.top.equalTo(forgotButton.snp.bottom).offset(30)
            make.centerX.equalTo(abnormalLabel)
        }
        
        leaveSecondButton.snp.makeConstraints { make in
            make.top.equalTo(abnormalSecondLabel.snp.bottom).offset(20)
            make.centerX.equalTo(abnormalLabel)
            make.size.equalTo(CGSize(width: 240, height: 40))
        }
        
        // between leave & forgot center with a clear separator.
        
        forgotSecondButton.snp.makeConstraints { make in
            
            
            //      if Dimension.isPhoneSE {
            make.top.equalTo(leaveSecondButton.snp.bottom).offset(20)
            
            make.centerX.equalTo(abnormalLabel)
            make.size.equalTo(CGSize(width: 240, height: 40))
            
            //      } else {
            //        make.top.equalTo(abnormalSecondLabel.snp.bottom).offset(20)
            //        make.leading.equalTo(centerClearSeparator.snp.trailing).offset(10)
            //        make.size.equalTo(CGSize(width: 160, height: 40))
            //      }
        }
        //--------- insert in scroll view ----------//
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(calender.snp.bottom)
            make.leading.trailing.bottom.equalTo(view)
        }
        scrollContentView.snp.makeConstraints { make in
            make.edges.width.equalTo(scrollView)
            make.top.equalTo(scrollView)
            make.leading.trailing.equalTo(view)
            make.bottom.equalTo(forgotSecondButton).offset(20) // the last one
        }
    }
}

extension CalenderViewController{
    private func setNaviLogo() {
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
    }
    
    private func topContentUI() {
        calender.addSubview(preMonthBtn)
        calender.addSubview(nextMonthBtn)
        
        topCalenderBgView.backgroundColor = Themes.starluxGold
        self.view.insertSubview(topCalenderBgView, at: 0)
    }
    
    private func calenderUI() {
        calender.dataSource = self
        calender.delegate = self
        calender.appearance.headerMinimumDissolvedAlpha = 0
        calender.appearance.todayColor = Themes.starluxGray
        calender.appearance.selectionColor = Themes.starluxGold
        calender.appearance.weekdayTextColor = .darkGray
        calender.appearance.titleWeekendColor = .gray
        calender.appearance.headerDateFormat = "MMM  yyyy"
        calender.appearance.headerTitleFont = Themes.font20Bold
        calender.appearance.caseOptions = .headerUsesUpperCase
        calender.appearance.headerTitleColor = .white
        calender.appearance.eventDefaultColor = .red
        calender.appearance.eventSelectionColor = Themes.warningRed
        calender.locale = Locale(identifier: "en_US_POSIX")
        
        calender.backgroundColor = .clear
        self.view.addSubview(calender)
    }
    
    private func middleContentUI() {
        view.addSubview(scrollView)
        scrollView.addSubview(scrollContentView)
        
        selectDayLabel.font = Themes.font17Bold
        selectDayLabel.textColor = Themes.starluxBlack
        scrollContentView.addSubview(selectDayLabel)
        //----------------------------------------------------//
        workTimeLabel.text = vm.workTimeLabel
        //    workTimeLabel.font = Themes.font20Bold
        workTimeLabel.textColor = Themes.starluxGold
        scrollContentView.addSubview(workTimeLabel)
        
        workTimeDesc.text = vm.workTimeDesc
        workTimeDesc.textColor = Themes.starluxBlack
        scrollContentView.addSubview(workTimeDesc)
        //----------------------------------------------------//
        breakTimeLabel.text = vm.breakTimeLabel
        //    breakTimeLabel.font = Themes.font20Bold
        breakTimeLabel.textColor = Themes.starluxGold
        scrollContentView.addSubview(breakTimeLabel)
        
        breakTimeDesc.text = vm.breakTimeDesc
        breakTimeDesc.textColor = Themes.starluxBlack
        scrollContentView.addSubview(breakTimeDesc)
        //----------------------------------------------------//
        orderMealLabel.text = vm.orderMealLabel
        orderMealLabel.textColor = Themes.starluxGold
        scrollContentView.addSubview(orderMealLabel)
        
        orderMealDesc.text = vm.orderMealDesc
        orderMealDesc.textColor = Themes.starluxBlack
        scrollContentView.addSubview(orderMealDesc)
        //----------------------------------------------------//
        
        grayLine.backgroundColor = .gray
        separatorLine.backgroundColor = .lightGray
        separatorLine2.backgroundColor = .lightGray
        separatorLine3.backgroundColor = .lightGray
        calender.addSubview(grayLine)
        scrollContentView.addSubview(separatorLine)
        scrollContentView.addSubview(separatorLine2)
        scrollContentView.addSubview(separatorLine3)
        
        punchTimeListLabel.text = vm.punchTimeListLabel
        punchTimeListLabel.textColor = Themes.starluxGold
        scrollContentView.addSubview(punchTimeListLabel)
    }
}



