//
//  CalendarViewModel.swift
//  posa
//
//  Created by JaxunC on 2018/9/17.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

// 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
enum CalenderDefine: Int {
    case unPunchTime = 0
    case late
    case leaveEarly
    case workOvertime
    case derelictionOfDuty
    var i18n: String{
        switch self{
        case .unPunchTime:
            return I18N.key("no_clock_in/out")
        case .late:
            return I18N.key("late")
        case .leaveEarly:
            return I18N.key("early_leave")
        case .workOvertime:
            return I18N.key("overtime_attendance")
        case .derelictionOfDuty:
            return I18N.key("absence_without_leave")
        }
    }
}

class CalendarViewModel: CalendarVMP{
    let mainCalenderService: MainCalenderServiceManager = Injector.inject()
    var monthlyPunchTimeDmArr: [MonthlyPunchTimeDm] = []
    var todayRecords: [String] = []
    var abnormalRecordList: [BaseAbnormalDm] = []
    var monthlyMealOrderRecordList: [BaseMealOrderDm] = []
    var abnormalDayList: [String] = []
    
    var punchTimeListLabel = I18N.key("details")
    var orderMealLabel = I18N.key("lunch_selection")
    var breakTimeDesc = "12:30 ~ 13:30"
    var breakTimeLabel = I18N.key("at_lunch")
    var orderMealDesc = ""
    var workTimeDesc = PunchTimeDefine.noData
    var workTimeLabel = I18N.key("attendance")
    var forgotSecondButtonTitle = I18N.key("failed_to_clock_in/out")
    var leaveSecondButtonTitle = I18N.key("request_leave")
    var detectText = "異常"  // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
    var forgotButtonTitle = I18N.key("failed_to_clock_in/out")
    var leaveButtonTitle = I18N.key("request_leave")
    
    var fetchYearMonth: PublishSubject<(String, String)> = PublishSubject()
    var cardData: Observable<String> = .empty()
    var mealOrder: Observable<String> = .empty()
    var cardDataMatch: Observable<Void> = .empty()
    let disposeBag = DisposeBag()
    
    init(){
        cardData = fetchYearMonth
            .flatMap{ [weak mainCalenderService] year, month in
                mainCalenderService?.fetchCardDataByMonth(year: year, month: month) ?? .empty()
            }
            .map({ [weak self] dms in
                self?.monthlyPunchTimeDmArr = dms
                let selectDay = Utils.getFullDateFormat(Date())
                return self?.updateWorkTime(selectDay) ?? ""
            })

        mealOrder = fetchYearMonth.flatMap{ [weak mainCalenderService] year, month in
            mainCalenderService?.fetchMealOrderByMonth(year: year, month: month) ?? .empty()
            }.do(onNext: { [weak self] dms in
                self?.monthlyMealOrderRecordList = dms
            })
            .map{ [weak self] _ in
                let selectedDay = Utils.getFullDateFormat(Date())
                return self?.monthlyMealOrderRecordList.first(where: { $0.date == selectedDay })?.selection
            }
            .flatMap{ Observable.from(optional: $0) }
        
        cardDataMatch = fetchYearMonth.flatMap{ [weak mainCalenderService] year, month in
            mainCalenderService?.fetchCardDataMatchByMonth(year: year, month: month) ?? .empty()
            }.do(onNext: { [weak self] dms in
                self?.abnormalRecordList = dms
                let list = self?.abnormalRecordList.map{ $0.date } ?? []
                self?.abnormalDayList += list
            })
            .map{ _ in Void() }
        
    }
    
    func showStatus(date: Date, first: (Bool) -> Void, second: (Bool) -> Void) -> (first: String, second: String){
        first(true)
        second(true)
        let selectedDate = Utils.getFullDateFormat(date)
        var firstText = ""
        var secondText = ""
        
        for baseAbnormalDto in abnormalRecordList {
            guard selectedDate == baseAbnormalDto.date else { continue }
            let firstAbnormal = baseAbnormalDto.status.first!
            let lastAbnormal = baseAbnormalDto.status.last!
            
            first(false)
            
            if firstAbnormal.cardType != lastAbnormal.cardType {
                second(false)
            }

            if firstAbnormal.code == 4 {
                second(false)
            }
            
            // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
            firstText = CalenderDefine(rawValue: firstAbnormal.code)?.i18n ?? ""
            secondText = CalenderDefine(rawValue: lastAbnormal.code)?.i18n ?? ""
            
        }
        return (firstText, secondText)
    }
    
    func updateWorkTime(_ selectDay: String) -> String{
        let recordList = monthlyPunchTimeDmArr.first(where: { selectDay == $0.fullDate})?.recordList
        todayRecords = recordList ?? []
        
        if todayRecords.first != todayRecords.last {
            let onWorkTime:String! = todayRecords.first
            let offWorkTime:String! = todayRecords.last
            
            return withOutSec(onWorkTime) + " ~ " + withOutSec(offWorkTime)
        }
        else if todayRecords.first != todayRecords.last {
            return todayRecords.first ?? "--:-- ~ --:--"
        }
        else{
            return "--:-- ~ --:--" //PunchTimeDefine.noData + " ~ " + PunchTimeDefine.noData
        }
    }
    
    func withOutSec(_ timeWithSec:String) -> String {
        return String(timeWithSec.prefix(5))
    }
}
