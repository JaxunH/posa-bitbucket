//
// Created by Jaxun on 2018/8/9.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit

struct FormHelper {
  static let bpmId = Pref().base64AccountId
  static let bpmTel = Pref().base64BPMTel
  static let bpmCode = Pref().uniqueBPMCode
  static let bpmCode2 = Pref().uniqueBPMCode2

  static func bpmPath(_ name:String) -> String {
    return "\(BuildConfig.bpmEndpoint)FM7_BPMPlus_\(name).aspx?" + // FM7_BPMPlus_CheckAction2.aspx?"
        "AccountID=\(bpmId!)&" +
        "IMEI=\(bpmCode!)&" +
        "CellNo=\(bpmTel!)&" +
        "DeviceToken=\(bpmCode2!)&" +
        "Act=0&" +
        "Lang=en-us"
  }
  
  static func forGotPath() -> String {
    return "&Identify=nUPL%2FOAe90hNhPnkj7grtA%3D%3D" +
            "&IdentifyName=%E5%BF%98%E5%88%B7%E5%8D%A1%E7%94%B3%E8%AB%8B%E5%96%AE"
  }
  static func forApplyLeavePath() -> String {
    return "&Identify=imgufhyPyUQ07Raj8eIyow%3D%3D" +
            "&IdentifyName=%E8%AB%8B%E5%81%87%E5%96%AE"
  }
}
