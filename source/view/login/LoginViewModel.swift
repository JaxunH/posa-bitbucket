//
//  LoginViewModel.swift
//  posa
//
//  Created by JaxunC on 2018/8/28.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import RxSwift

class LoginViewModel: LoginVMP{
    var emailText: ReplaySubject<String> = ReplaySubject.create(bufferSize: 1)

    var passwordText: ReplaySubject<String> = ReplaySubject.create(bufferSize: 1)

    var didPressLoginBtn: PublishSubject<Void> = PublishSubject()

    var isLogin: ReplaySubject<Bool> = ReplaySubject.create(bufferSize: 1)

    var punchType: ReplaySubject<Int> = ReplaySubject.create(bufferSize: 1) // punch on = 0, punch off = 1

    var willNavigateViewController: Observable<Bool> = .empty()

    var errorMsgEmptyEmail: Observable<String> = .empty()

    var errorMsgEmptyPassword: Observable<String> = .empty()

    var errorMsgResult: Observable<String> = .empty()

    var suffixEmailString: Observable<String> = .empty()

    var loginType: Observable<Int> = .empty()
    
    fileprivate lazy var loginService: AccountServiceManager = Injector.inject()

    init(){

        errorMsgEmptyEmail = Observable.of("E-mail can not be empty")

        errorMsgEmptyPassword = Observable.of("Password can not be empty")

        suffixEmailString = Observable.of("@starlux-airlines.com")

        let zip = Observable.combineLatest(emailText, passwordText)

        let optionalDto = didPressLoginBtn
            .withLatestFrom(zip)
            .flatMap { [weak self] (email, password) in
                return self?.loginService.fetchAlias(account: email, password: password) ?? .empty()
            }
            .share(replay: 1)

        errorMsgResult = optionalDto
            .map{ (success, errorMsg) in success ? "" : errorMsg }

        willNavigateViewController = optionalDto
            .map{ (success, errorMsg) in success }
        
        loginType = zip
            .map{ (email, pw) -> Int in
                if email.isEmpty == true {
                    return 0
                }
                else if pw.isEmpty == true {
                    return 1
                }
                else{
                    return 2
                }
            }
        
    }
}
