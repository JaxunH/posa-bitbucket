//
//  LoginTest.swift
//  posa
//
//  Created by JaxunC on 2018/8/29.
//  Copyright © 2018 STARLUX. All rights reserved.
//

import Quick
import RxSwift
import RxTest
import RxBlocking
@testable import posa

class LoginTest: QuickSpec {
    
    var vm: LoginVMP!
    var disposeBag = DisposeBag()
    
    override func spec() {
        
        describe("login logic") {
            beforeEach {
                self.vm = LoginViewModel()
                self.disposeBag = DisposeBag()
            }
            
            it("exist email, exist password") {
                self.loginPressed(email: "jaxunhuang@starlux-airlines.com",
                                  password: "Zaq12wsx",
                                  loginSuccess: true)

            }

            it("failure email, exist password") {
                self.loginPressed(email: "@starlux-airlines.com",
                                  password: "Zaq12wsx",
                                  loginSuccess: false)
            }

            it("exist email, failure password") {
                self.loginPressed(email: "jaxunhuang@starlux-airlines.com",
                                  password: "usick",
                                  loginSuccess: false)
            }
            
            it("failure email, failure password") {
                self.loginPressed(email: "@starlux-airlines.com",
                                  password: "idunno",
                                  loginSuccess: false)
            }
        }
        
    }
    
    private func loginPressed(email: String, password: String, loginSuccess: Bool) {
        let input: Observable<(String, String)> = Observable.of((email, password))
        var output: Observable<Bool> = .empty()
        let expected: Bool = loginSuccess
        
        //
        input.map{ $0.0 }
            .bind(to: vm.emailText).disposed(by: disposeBag)
        
        input.map{ $0.1 }
            .bind(to: vm.passwordText).disposed(by: disposeBag)

        input.map{ _ in }
            .delay(1, scheduler: MainScheduler.instance)
            .bind(to: vm.didPressLoginBtn).disposed(by: disposeBag)

        //
        let errMsgOutput = vm.errorMsgResult.map { !$0.isEmpty }.delay(0.1, scheduler: MainScheduler.instance)
        output = Observable.merge(vm.willNavigateViewController, errMsgOutput)
        
        //
        let r = try! output.toBlocking(timeout: 2).toArray()
        XCTAssertEqual(r, [expected, !expected])
    }
    
}
