//
// Created by STARLUX on 1/3/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import Kingfisher
import SwiftTheme

class EmptyCell: UIKit.UICollectionViewCell {
  private var coverImageView:UIImageView
  private var emptyText:UILabel


  override init(frame:CGRect) {
    coverImageView = UIImageView(frame: .zero)
    emptyText = UILabel(frame: .zero)

    super.init(frame: frame)
    contentView.theme_backgroundColor = Themes.backgroundColor
    contentView.addSubview(coverImageView)
    contentView.addSubview(emptyText)

    coverImageView.image = UIImage(imageLiteralResourceName: "icon_remind_smile")
    emptyText.theme_textColor = Themes.themedTextColorLight

    if let image = coverImageView.image {
      coverImageView.snp.makeConstraints { (make) -> Void in
        make.top.equalToSuperview().offset(10)
        make.leading.equalToSuperview().offset(10)
        make.height.equalTo(image.size.height)
        make.width.equalTo(image.size.width)
      }
      emptyText.snp.makeConstraints({ (make) -> Void in
        make.leading.equalTo(coverImageView.snp.trailing).offset(Dimension.MATERIAL_SPACING_8)
        make.trailing.equalToSuperview()
        make.centerY.equalTo(coverImageView)
      })

      emptyText.text = NSLocalizedString("nothing_here", comment: "")
    }


  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
