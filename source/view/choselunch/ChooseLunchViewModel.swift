//
// Created by Jaxun on 2018/8/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import PKHUD
import Alamofire


protocol ChooseLunchVMP {
    
    //var dataSource: ReplaySubject<ChooseLunchDataSource> //observer, observable
    
    var chooseTitle: Observable<String?> { get } // chose lunch, step1: assert = "chose dinner"

    var vegetableNameTitle: Observable<String?> { get }
    var vegetableBtnImageName: Observable<String> { get }
    var didPressVegetableBtn: PublishSubject<Void> { get }
    
    var meatNameTitle: Observable<String?> { get }
    var meatBtnImageName: Observable<String> { get }
    var didPressMeatBtn: PublishSubject<Void> { get }
    
    var selfPrepareNameTitle: Observable<String?> { get }
    var selfPrepareBtnImageName: Observable<String> { get }
    var didPressSelfPrepareBtn: PublishSubject<Void> { get }

    var hudShow: PublishSubject<HUDContentType> { get }
    var hudFlash: PublishSubject<HUDContentType> { get }

    var didClose: PublishSubject<Void> { get }
}

class ChooseLunchViewModel: ChooseLunchVMP {
    
    private let serviceManager: ChoseLunchServiceManager = Injector.inject()
    
    private let orderType = PublishSubject<ChooseLunchApis.OrderType>()
    
    let disposeBag = DisposeBag()
    
    var chooseTitle: Observable<String?> = .empty()
    
    var vegetableNameTitle: Observable<String?> = .empty()
    
    var vegetableBtnImageName: Observable<String> = .empty()
    
    let didPressVegetableBtn: PublishSubject<Void> = PublishSubject()
    
    var meatNameTitle: Observable<String?> = .empty()
    
    var meatBtnImageName: Observable<String> = .empty()
    
    let didPressMeatBtn: PublishSubject<Void> = PublishSubject()
    
    var selfPrepareNameTitle: Observable<String?> = .empty()
    
    var selfPrepareBtnImageName: Observable<String> = .empty()
    
    let didPressSelfPrepareBtn: PublishSubject<Void> = PublishSubject()

    var hudShow: PublishSubject<HUDContentType> = PublishSubject()

    var hudFlash: PublishSubject<HUDContentType> = PublishSubject()

    var didClose: PublishSubject<Void> = PublishSubject()

    init(){

        chooseTitle = Observable.of("Lunch Selection")
        
        vegetableNameTitle = Observable.of("Vegetarian")
        vegetableBtnImageName = Observable.of("icon_vegi")
        
        meatNameTitle = Observable.of("Regular")
        meatBtnImageName = Observable.of("icon_meat")

        selfPrepareNameTitle = Observable.of("Self-prepared")
        selfPrepareBtnImageName = Observable.of("icon_self")
        
        didPressSelfPrepareBtn
            .map{ _ in ChooseLunchApis.OrderType.bySelf }
            .bind(to: orderType).disposed(by: disposeBag)
        
        didPressMeatBtn
            .map{ _ in ChooseLunchApis.OrderType.meat }
            .bind(to: orderType).disposed(by: disposeBag)
        
        didPressVegetableBtn
            .map{ _ in ChooseLunchApis.OrderType.vegetarian }
            .bind(to: orderType).disposed(by: disposeBag)
        
        orderType
            .do(onNext: { [weak hudShow] _ in
                hudShow?.onNext(.progress)
            })
            .flatMap{ [weak self] type in self?.serviceManager.orderLunchMeal(type: type) ?? .empty() }
            .do(onNext: { [weak self] (isSuccess, orderedMeal) in
                guard let `self` = self else { return }
                if isSuccess {
                    self.hudFlash.onNext(.labeledSuccess(title: I18N.key("success"), subtitle: orderedMeal + " ordered"))
                } else {
                    self.hudFlash.onNext(.labeledError(title: "Failure", subtitle: nil))
                }
            })
            .map{ _ in }
            .bind(to: didClose).disposed(by: disposeBag)
    }
    
    deinit {
        print("deinit", self)
    }
}

