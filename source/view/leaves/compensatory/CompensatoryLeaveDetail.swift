//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import PKHUD

class CompensatoryLeaveDetail: UIViewController {
    //UIs
    fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
    fileprivate let tableView = UITableView(frame: .zero, style: .plain)
    ////Datas
    fileprivate let leaveStatisticsService: LeaveStatisticsServiceManager = Injector.inject()
    fileprivate var choseYear = String()
    fileprivate var results: [CompensatoryLeaveDm] = []
    
    //MARK:- Methods
    init(choseYear:String) {
        super.init(nibName: nil, bundle: nil)
        self.choseYear = choseYear
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
        setNaviLogo()
        setTableViewUI()
        setRx()
    }
    
    private func setTableViewUI() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = Themes.starluxBg
        tableView.allowsSelection = false
        tableView.register(CompensatoryLeaveBodyCell.self,
                           forCellReuseIdentifier: NSStringFromClass(CompensatoryLeaveBodyCell.self))
        
        view.addSubview(tableView)
        
        logoImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }
        tableView.snp.makeConstraints { maker in
            maker.edges.equalTo(view)
        }
    }
    
    private func setNaviLogo() {
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
    }
    
    private func setRx(){
        HUD.flash(.progress)
        leaveStatisticsService.fetchCompensatory(year: choseYear)
            .subscribe(onNext: { [weak self] dmArr in
                self?.results = dmArr
                self?.tableView.reloadData()
                HUD.hide()
            }).disposed(by: super.disposeBag)
    }
}


extension CompensatoryLeaveDetail: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CompensatoryLeaveBodyCell.self),
                                                 for: indexPath)
        
        let aCell = cell as? CompensatoryLeaveBodyCell
        aCell?.backgroundColor = Themes.starluxBg
        aCell?.bindCompensatory(results[indexPath.row])
        aCell?.appendLine()
        
        return cell
    }
}

