import UIKit
import SnapKit
import Alertift
import PKHUD


class TypeOfLeaveController: CollapsibleTableSectionViewController {

    enum LeaveType: Int{
        case bereavement
        case maternity
    }
    
    fileprivate var leaveAllResult: [TypeOfLeaveDm] = []
    fileprivate var choseYear = ""
    fileprivate var leaveType: LeaveType!
    
    fileprivate lazy var logoImg: UIImageView = {
        $0.image = UIImage(named: "title_logo")
        $0.contentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        $0.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }
        return $0
    }(UIImageView(frame: .zero))

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    init(choseYear: String, type: LeaveType) {
        super.init(nibName: nil, bundle: nil)
        self.choseYear = choseYear
        self.leaveType = type
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutSubview()
        self.delegate = self
        self.fetchApi()
        super._tableView.register(TypeOfLeaveCell.self, forCellReuseIdentifier: TypeOfLeaveCell.description())
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
    }

    private func layoutSubview() {
        view.backgroundColor = Themes.starluxGold
        navigationItem.titleView = logoImg
    }

}

extension TypeOfLeaveController: CollapsibleTableSectionDelegate {

    func numberOfSections(_ tableView: UITableView) -> Int {
        return leaveAllResult.count
    }

    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TypeOfLeaveCell.description(), for: indexPath)
        
        let aCell = cell as? TypeOfLeaveCell
        aCell?.bind(leaveAllResult[indexPath.section])
        aCell?.backgroundColor = Themes.starluxLight
        aCell?.selectionStyle = .none
        
        return cell
    }

    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return leaveAllResult[section].typeOfLeave
    }

    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaveAllResult.count == 0 ? 0 : 1
    }

    func collapsibleTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return false
    }

    func shouldCollapseOthers(_ tableView: UITableView) -> Bool{
        return false
    }

}

private extension TypeOfLeaveController{
    func fetchApi(){
        let leaveStatisticsService: LeaveStatisticsServiceManager = Injector.inject()
        leaveStatisticsService.fetchBereavement(year: choseYear)
            .subscribe(onNext: { [weak self] dtos in
                guard let `self` = self else { return }
                self.leaveAllResult = dtos
                self._tableView.reloadData()
            }).disposed(by: disposeBag)
    }
}
