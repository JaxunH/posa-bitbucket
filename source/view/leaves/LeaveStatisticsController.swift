import UIKit
import SnapKit
import Alertift
import PKHUD
import RxSwift


class LeaveStatisticsController: UIViewController {
    
    //UIs
    fileprivate let yearTitle = UILabel(frame: .zero)
    fileprivate let attendanceRecordView = UIView(frame: .zero)
    fileprivate let expandableView = CollapsibleTableSectionViewController()
    fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
    
    //Data -> VM
    let fetch: PublishSubject<String> = PublishSubject()
    
    fileprivate let pref = Pref()
    fileprivate let leaveStatisticsService: LeaveStatisticsServiceManager = Injector.inject()
    
    fileprivate var leaveAllResult: [EmployeeAllLeaveDm] = []

    fileprivate lazy var currentYear = Utils.getYear(Date())
    
    fileprivate lazy var chooseYearString: String = {
        let onBoard = pref.aliasOnBoardDay ?? currentYear
        return String(onBoard.suffix(4)) // ex: "day_onboard": "Apr 02 2018"
    }()
    
    lazy var displayYears: [String] = {
        let currentYear = Utils.getYear(Date())
        //let currentYear = "2050" //for test multi years
        
        guard var standardYear = Int(currentYear), let onBoardYear = Int(chooseYearString) else { return [] }
        return (onBoardYear ... standardYear).reversed().map { String($0) }
    }()
    
    //MARK:- Methods
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setRx()
        setLayout()
        fetchApi(choseYear: currentYear)
    }
    
    @objc func clickChoseYear() {
        let displayYears = self.displayYears
        
        Alertift.actionSheet(message: nil)
            .actions(displayYears) //["2018","2017"...]
            .action(.cancel("cancel"))
            .finally { [weak self] action, index in
                guard let `self` = self, action.style != .cancel else { return }
                self.chooseYearString = displayYears[index]
                self.fetchApi(choseYear: displayYears[index])
                self.yearTitle.text = displayYears[index] + " " + I18N.key("attendance_record")
            }
            .show()
    }
}

extension LeaveStatisticsController: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return leaveAllResult.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LeaveStatisticsBodyCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(
            LeaveStatisticsBodyCell.self)) as? LeaveStatisticsBodyCell ??
            LeaveStatisticsBodyCell(style: .default, reuseIdentifier: NSStringFromClass(LeaveStatisticsBodyCell.self))
        
        let leave = leaveAllResult[indexPath.section]
        cell.bind(leave)
        cell.backgroundColor = Themes.starluxLight
        
        cell.selectionStyle = .none
        return cell
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return leaveAllResult[section].leaveNameEn
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaveAllResult.count == 0 ? 0 : 1
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
    func shouldCollapseOthers(_ tableView: UITableView) -> Bool{
        return false
    }
    
    func handleSectionPressed(_ section: Int) -> Bool {
        switch leaveAllResult[section].leaveNameEn{
        case "Compensatory Leave":
            let controller = CompensatoryLeaveDetail(choseYear: chooseYearString)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Menstrual Leave":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .menstrual)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Bereavement Leave":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .bereavement)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Maternity Leave":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .maternity)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Marriage Leave":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .marriage)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Prenatal Visit":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .prenatal)
            navigationController!.pushViewController(controller, animated: true)
            return true
        case "Paternity Leave":
            let controller = MenstrualLeaveDetail(choseYear: chooseYearString, type: .paternity)
            navigationController!.pushViewController(controller, animated: true)
            return true
        default:
            return false
        }
    }
}

private extension LeaveStatisticsController{
    
    func fetchApi(choseYear: String) {
        HUD.show(.progress)
        fetch.onNext(choseYear)
    }
}

private extension LeaveStatisticsController{
    func setRx(){        
        fetch.flatMap{ [weak self] choose in self?.leaveStatisticsService.fetchAll(year: choose) ?? .empty() }
            .subscribe(onNext: { [weak self] dmArr -> Void in
                self?.leaveAllResult = dmArr
                self?.expandableView._tableView.reloadData()
                HUD.hide()
                
            }).disposed(by: super.disposeBag)
    }
    
    func setLayout() {
        //setNaviLogo
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
        
        //setLayout
        view.addSubview(expandableView.view)
        view.addSubview(attendanceRecordView)
        expandableView.delegate = self
        attendanceRecordView.backgroundColor = Themes.starluxGold
        
        //setConstraints
        logoImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }
        
        //
        yearTitle.text = currentYear + " " + I18N.key("attendance_record")
        yearTitle.textColor = .white
        yearTitle.font = Themes.font20
        
        let arrowIcon = UIImageView(image: UIImage(named: "right_arrow"))
        arrowIcon.contentMode = .scaleAspectFit
        
        let attendanceRecordViewTap = UITapGestureRecognizer(target: self, action: #selector(clickChoseYear))
        
        attendanceRecordView.addSubview(yearTitle)
        attendanceRecordView.addSubview(arrowIcon)
        attendanceRecordView.addGestureRecognizer(attendanceRecordViewTap)
        
        yearTitle.snp.makeConstraints { maker in
            maker.top.equalTo(attendanceRecordView).offset(10)
            maker.centerX.equalTo(view)
            maker.size.equalTo(CGSize(width: 230, height: 30))
        }
        
        arrowIcon.snp.makeConstraints { maker in
            maker.size.equalTo(CGSize(width: 24, height: 24))
            maker.leading.equalTo(yearTitle.snp.trailing).offset(4)
            maker.centerY.equalTo(yearTitle)
        }
        
        attendanceRecordView.snp.makeConstraints { maker in
            maker.top.leading.trailing.equalTo(0)
            maker.height.equalTo(42)
        }
        
        expandableView.view.snp.makeConstraints { maker in
            maker.top.equalTo(attendanceRecordView.snp.bottom).offset(0)
            maker.leading.trailing.bottom.equalToSuperview()
        }
    }
}
