//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import PKHUD

class MenstrualLeaveDetail: UIViewController {
    
    //UIs
    fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
    fileprivate let tableView = UITableView(frame: .zero, style: .plain)
    
    //Datas -> VM
    enum LeaveType: Int {
        case menstrual
        case marriage
        case paternity
        case prenatal
        case bereavement
        case maternity
    }
    
    fileprivate let leaveStatisticsService: LeaveStatisticsServiceManager = Injector.inject()
    fileprivate var choseYear = ""
    fileprivate var results: [Any] = []
    fileprivate var leaveType: LeaveType!
    
    
    //MARK:- Methods
    init(choseYear: String, type: LeaveType) {
        super.init(nibName: nil, bundle: nil)
        
        self.choseYear = choseYear
        self.leaveType = type
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
        setNaviLogo()
        setTableViewUI()
        fetchApi(leaveType)
    }
    
    private func fetchApi(_ type: LeaveType) {
        HUD.flash(.progress)
        
        switch type {
        case .menstrual:
            leaveStatisticsService.fetchMenstrual(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        case .marriage:
            leaveStatisticsService.fetchMarriage(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        case .paternity:
            leaveStatisticsService.fetchPaternity(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        case .prenatal:
            leaveStatisticsService.fetchPrenatalVisit(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        case .maternity:
            leaveStatisticsService.fetchMaternity(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        case .bereavement:
            leaveStatisticsService.fetchBereavement(year: choseYear)
                .subscribe(onNext: { [weak self] dmArr -> Void in
                    self?.results = dmArr
                    self?.tableView.reloadData()
                    HUD.hide()
                }).disposed(by: disposeBag)
        }
    }
    
    private func setTableViewUI() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = Themes.starluxBg
        tableView.allowsSelection = false
        
        registerCell()
        
        view.addSubview(tableView)
        
        logoImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }
        tableView.snp.makeConstraints { maker in
            maker.edges.equalTo(view)
        }
    }
    
    private func setNaviLogo() {
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
    }
    
    private func registerCell() {
        switch leaveType{
        case .menstrual?:
            tableView.register(MenstrualLeaveBodyCell.self, forCellReuseIdentifier: getReusableId())
        case .maternity?, .bereavement?:
            tableView.register(TypeOfLeaveCell.self, forCellReuseIdentifier: getReusableId())
            
        case .marriage?, .paternity?, .prenatal?:
            fallthrough
        default:
            tableView.register(DateOfEventCell.self, forCellReuseIdentifier: getReusableId())
        }
    }
    
    private func getReusableId() -> String{
        switch leaveType{
        case .menstrual?:
            return NSStringFromClass(MenstrualLeaveBodyCell.self)
            
        case .maternity?, .bereavement?:
            return NSStringFromClass(TypeOfLeaveCell.self)
            
        case .marriage?, .paternity?, .prenatal?:
            fallthrough
            
        default:
            return NSStringFromClass(DateOfEventCell.self)
        }
    }
    
    private func bind(from cell: UITableViewCell, data: Any){
        switch leaveType{
        case .menstrual?:
            let aCell = cell as? MenstrualLeaveBodyCell
            aCell?.backgroundColor = Themes.starluxBg
            aCell?.bind(data as! MenstrualLeaveDm)
            aCell?.appendLine()
            
        case .marriage?, .paternity?, .prenatal?:
            let aCell = cell as? DateOfEventCell
            aCell?.backgroundColor = Themes.starluxBg
            aCell?.bind(data as! DateOfEventLeaveDm)
            aCell?.appendLine()
            
        case .maternity?, .bereavement?:
            let aCell = cell as? TypeOfLeaveCell
            aCell?.backgroundColor = Themes.starluxBg
            aCell?.bind(data as! TypeOfLeaveDm)
            aCell?.appendLine()
            
        default:
            break
        }
    }
}

extension MenstrualLeaveDetail: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let reusableId = getReusableId()
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableId, for: indexPath)
        bind(from: cell, data: results[indexPath.row])
        return cell
    }
}
