//
//  AccountDm.swift
//  posa
//
//  Created by JaxunC on 2018/9/12.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

struct AccountDm: Codable {
    let token: String
    let empNo: String
    let empName: String
    let jobCname: String //let jobTitle:String
    let jobEname: String //let jobEnTitle:String
    let enFullName: String //let eName:String
    let depCname: String //let departmentName:String
    let depEname: String //let departmentEName:String
    let dayOnboard: String //let onBoardDay:String
    let empSex: String //let sex:String
    let empTel: String //let tel:String
    let site: String //let floorSite:String
    let photo: String //let aliasIconUrl:String
}

