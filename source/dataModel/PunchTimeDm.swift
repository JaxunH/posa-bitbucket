//
//  PunchTimeDm.swift
//  posa
//
//  Created by JaxunC on 2018/9/14.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

struct RecordDm: Codable {
    let workOnRecordList: [String]
    let workOffRecordList: [String]
    
    var firstCheckIn: String {
        return workOnRecordList.first ?? PunchTimeDefine.noData
    }
    
    var lastCheckOut: String {
        return workOffRecordList.last ?? PunchTimeDefine.noData
    }
}

struct WifiDm: Codable {
    let ssid:[String]
}

struct PunchTimeDm: Codable {
    let punchedServerTime:String?
    let resultMsg:String?
    var isSuccess: Bool { return resultMsg == nil }
    
    enum CodingKeys: String, CodingKey {
        case punchedServerTime = "sys_time"
        case resultMsg = "msg"
    }
}

struct BPMBadgeDm: Codable{
    let badgeForApprove: Int
    let badgeForTrack: Int
}

struct UpdateVersionDm: Codable {
    let isForce : Bool
    let needUpdate : Bool
    let infoMsg: String
    let currentVersion: String

    enum CodingKeys: String, CodingKey {
        case isForce = "is_force_update"
        case needUpdate = "is_need_update"
        case currentVersion = "current_version"
        case infoMsg = "info_msg"
    }
}
