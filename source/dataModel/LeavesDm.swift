//
// Created by JaxunC on 2018-09-13.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

protocol LeaveDMP {
    var duration: String { get }
    var accrued: Double { get }
    var used: Double { get }
    var available: Double { get }
}

struct EmployeeAllLeaveDm: LeaveDMP, Codable {
    var duration: String
    var accrued: Double
    var used: Double
    var available: Double
    var signing: Double
    let leaveName: String
    let leaveYear: String
    let leaveUnitName: String
    let leaveUnit: String
    let leaveNameEn: String
    
    enum CodingKeys: String, CodingKey{
        case leaveUnit = "leaveUnitCode"
        case leaveUnitName = "CLeaveUnit"
        case leaveNameEn = "ELeaveName"
        case leaveName
        case leaveYear
        case duration
        case accrued
        case used
        case available
        case signing
    }

}

struct CompensatoryLeaveDm: LeaveDMP, Codable {
    var duration: String
    var accrued: Double
    var used: Double
    var available: Double
    let dateOfEvent: String
    let leaveName: String
    let leaveNameEn: String
    
    enum CodingKeys: String, CodingKey{
        case leaveNameEn = "ELeaveName"
        case leaveName
        case dateOfEvent
        case duration
        case accrued
        case used
        case available
    }
}

struct TypeOfLeaveDm: LeaveDMP, Codable {
    var duration: String
    var accrued: Double
    var used: Double
    var available: Double
    var signing: Double
    let typeOfLeave: String
    let dateOfEvent: String
}

struct MenstrualLeaveDm: LeaveDMP, Codable {
    var duration: String //date == duration
    var accrued: Double
    var used: Double
    var available: Double
    var signing: Double
    let leavePeriod: String
}

struct DateOfEventLeaveDm: LeaveDMP, Codable {
    var duration: String
    var accrued: Double
    var used: Double
    var available: Double
    var signing: Double
    let dateOfEvent: String
}
