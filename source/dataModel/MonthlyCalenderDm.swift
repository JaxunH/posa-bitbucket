//
// Created by JaxunC on 2018/9/11.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

struct Status: Codable {
    let cardType: Int
    let cardShould: String
    let cardTime: String
    let code: Int
}

struct MonthlyPunchTimeDm: Codable {
    let fullDate: String
    let date: String
    let recordList: [String]
}

struct BaseAbnormalDm: Codable {
    let date: String
    let status: [Status]
}

struct BaseMealOrderDm: Codable {
     let date:String
     let selection:String
}
