//
//  ServiceHandler.swift
//  posa
//
//  Created by JaxunC on 2018/9/10.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import Crashlytics

final class ServiceHandler<API: RequestParams>{
    
    subscript(api: API) -> Observable<Result> { return request(api: api) }
    
    struct Result {
        enum ResultCode: Int{
            case success = 1
            case failure = 3
            case serverError = 999
        }
        var url: String?
        var statusCode: Int
        var code: Int
        var message: String
        var jsonData: Any?
        var isSuccess: Bool { return code == ResultCode.success.rawValue }
   
        init(httpRes: HTTPURLResponse, res: Any){
            guard let dict = res as? [String: Any?] else { fatalError("downcast any to dict.") }
            statusCode = httpRes.statusCode
            code = dict["code", default: ResultCode.failure.rawValue] as! Int
            message = dict["msg"] as? String ?? ""
            if let val = dict["data"], val != nil{
                jsonData = val
            }else{
                jsonData = dict
            }
            url = httpRes.url?.absoluteString
        }
        
        init(error: Error){
            let nsError = error as NSError
            statusCode = nsError.code
            code = ResultCode.serverError.rawValue
            message = nsError.localizedDescription
        }
    }
    
    init(){
        Alamofire.SessionManager.default.delegate.sessionDidReceiveChallenge = { session, challenge in
            return (URLSession.AuthChallengeDisposition.useCredential,
                    URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
    }
}

//MARK: Alamofile
private extension ServiceHandler{
    
    var endpoint: String {
        return BuildConfig.endpoint
    }
    
    var headers: [String: String] {
        var header:[String: String] = [:]
        header["Content-Type"] = BuildConfig.ottBrand
        header["token"] = Pref().aliasToken

        return header
    }
    
    func request(api: API) -> Observable<Result> {
        return RxAlamofire
            .requestJSON(api.method,
                         endpoint + api.path,
                         parameters: api.params,
                         encoding: URLEncoding.default,
                         headers: headers)
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .map{ (httpRes, res) in Result(httpRes: httpRes, res: res) }
            .catchError{ error in
                let httpCodeError = ApiError.convertError(error)
                toastApiErrorHandler(apiError: httpCodeError)
                return Observable.of(Result(error: error))
            }
            .do(onNext: { [weak self] r in
                self?.printResult(api, from: r)
                self?.uploadError(api, from: r)
            })
            .observeOn(MainScheduler.instance)
    }
}

//MARK: Logs
fileprivate extension ServiceHandler{
    func createLog(api: API, result: Result) -> [String: Any] {
        var answerParams: [String: Any] = [:]
        answerParams["userId"] = Pref().aliasUniqueId
        answerParams["userName"] = Pref().aliasNameEn
        answerParams["requestedParams"] = api.params
        answerParams["requestedResponse"] = result.jsonData
        answerParams["requestedPath"] = api.path
        return answerParams
    }
    
    func printResult(_ api: API, from r: Result){
        //Internal Log
        log.debug("===========================")
        log.debug("RxAlamofireResult --- Start")
        log.debug("enumApi   : \(api)")
        log.debug("apiMethod : \(api.method)")
        log.debug("apiPath   : \(api.path)")
        log.debug("apiParams : \(String(describing: api.params))")
        log.debug("\(r)")
        log.debug("RxAlamofireResult --- End")
        log.debug("===========================\n")
    }

    func uploadError(_ api: API, from r: Result){
        guard r.statusCode != 200 else { return }
        //To create a fabric non-fatal issue after re-launch,
        
        let answerParams = createLog(api: api, result: r)
        let error = NSError(domain: api.path, code: r.statusCode, userInfo: answerParams)
        Crashlytics.sharedInstance().recordError(error)
        
        //todo: online log: CLSLogv() https://www.jianshu.com/p/f20f0e773fc0
    }
}

extension ServiceHandler.Result {
    
    /// Decode jsonData to [DecodableTypeValue]
    ///
    /// We usually use it into [*DataModel]
    /// - Parameter type: (class or struct inherit Decodable).self
    /// - Returns: [DecodableTypeValue]
    static func toArray<T: Decodable>(_ type: T.Type) -> ( (ServiceHandler.Result) -> [T] ){
        return { r in
            let decoder = JSONDecoder()
            //decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard
                let dictArr = r.jsonData as? [Dictionary<String, Any>],
                let decoded = try? decoder.decodeToArray(type, from: dictArr)
                else { return [] }
            
            return decoded
        }
    }
    
    static func to<T: Decodable>(_ type: T.Type) -> ( (ServiceHandler.Result) -> T? ){
        return { r in
            let decoder = JSONDecoder()
            //decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard
                let dict = r.jsonData as? Dictionary<String, Any>,
                let decoded = try? decoder.decode(type, from: dict)
                else { return nil }
            
            return decoded
        }
    }
}
