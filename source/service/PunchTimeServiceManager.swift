//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct PunchTimeDefine {
    static let firstTime = I18N.key("in")
    static let lastTime = I18N.key("out")
    static let noData = "--:--:--"
    static let punchTimeOn = I18N.key("clock_in")
    static let punchTimeOff = I18N.key("clock_out")
    static let formApply = I18N.key("application")
    static let formSign = I18N.key("approval")
    static let formTrack = I18N.key("tracing")

    static let yearDayFormat = "MMM  dd  yyyy"
    static let yearNtimeFormat = "yyyy/MM/dd HH:mm:ss"
    static let timeFormat = "HH : mm : ss"
    static let weekDayFormat = "EEE"
    static let todayFormat = "yyyy/MM/dd"
}

class PunchTimeServiceManager {

    private let pref = Injector.injectPref()
    
    private var accountId: String { return pref.aliasToken ?? "" }
    
    private let handler = ServiceHandler<PunchTimeApis>()
    
    func sendPushToken() -> Observable<Void> {
        guard let token = pref.fcmPushToken else { return .empty() }
        return handler[.token(accountId: accountId, tokenId: token)]
            .map{ _ in Void() }
    }

    func getBPMInfo() -> Observable<Void> {
        let mobile = pref.aliasTel ?? ""
        let code = pref.uniqueBPMCode ?? ""
        return handler[.getBPMInfo(accountId: accountId, mobile: mobile, code: code)]
            .do(onNext: { [weak self] (r) in
                guard
                    let dicts = r.jsonData as? [String: AnyObject],
                    let dict = dicts["xml"]?["group"] as? [String: String]
                    else { return }
                self?.pref.base64AccountId = dict["AccountID"]?.urlEncoded()
                self?.pref.base64BPMTel = dict["Mobile"]?.urlEncoded()
                self?.pref.uniqueBPMCode = dict["IMEICode"]?.urlEncoded()
                self?.pref.uniqueBPMCode2 = dict["DeviceToken"]?.urlEncoded()
            })
            .map{ _ in Void() }
    }

    func getFormBadgeCounts() -> Observable<BPMBadgeDm> {
        return handler[.getBadgeCount(accountId: accountId)]
            .map(ServiceHandler.Result.to(BPMBadgeDm.self))
            .flatMap{ Observable.from(optional: $0) }
    }

    func fetchWifiList() -> Observable<WifiDm> {
        return handler[.getWifiList]
            .map(ServiceHandler.Result.to(WifiDm.self))
            .flatMap{ Observable.from(optional: $0) }
    }

    func fetchTodayRecords(today: String) -> Observable<RecordDm> {
        return handler[.getTodayCard(accountId: accountId, today: today)]
            .map(ServiceHandler.Result.to(RecordDm.self))
            .flatMap{ Observable.from(optional: $0) }
    }

    func fetchUpdateInfo() -> Observable<UpdateVersionDm> {
        //let currentVersion = "0.0.15" // version num + build num
        let currentVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
        return handler[.updateInfo(os: 0, versionNum: currentVersion)] //ios 0, android 1
            .map(ServiceHandler.Result.to(UpdateVersionDm.self))
            .flatMap { Observable.from(optional: $0 ) }
    }

    /**
      reset punch time device id api:
      http://10.20.11.71:3000/api/updateDeviceID
      params: ep_no = "account id", device_id =""
     */
    func punchTimeWork(punchType: Int) -> Observable<PunchTimeDm> {
        guard let deivceId = Pref().aliasUniqueId else { return .empty() }
        return handler[.clock(accountId: accountId, deviceId: deivceId, punchType: punchType) ]
            .map(ServiceHandler.Result.to(PunchTimeDm.self))
            .flatMap{ Observable.from(optional: $0) }
    }
}
