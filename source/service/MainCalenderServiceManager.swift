//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift


class MainCalenderServiceManager {
    
    private let handler: ServiceHandler<CalenderApis> = ServiceHandler()
    
    func fetchCardDataByMonth(year:String, month:String) -> Observable<[MonthlyPunchTimeDm]> {
        let params = (year: year, month: month)
        return handler[.fetchCardDataByMonth(params)]
            .map(ServiceHandler.Result.toArray(MonthlyPunchTimeDm.self))
    }
    
    func fetchCardDataMatchByMonth(year:String, month:String) -> Observable<[BaseAbnormalDm]> {
        let params = (year: year, month: month)
        return handler[.fetchCardDataMatchByMonth(params)]
            .map(ServiceHandler.Result.toArray(BaseAbnormalDm.self))
    }
    
    func fetchMealOrderByMonth(year:String, month:String) -> Observable<[BaseMealOrderDm]> {
        let params = (year: year, month: month)
        return handler[.fetchMealOrderByMonth(params)]
            .map(ServiceHandler.Result.toArray(BaseMealOrderDm.self))
    }
}
