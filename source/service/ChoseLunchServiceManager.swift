//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class ChoseLunchServiceManager {
    
    private lazy var pref = Injector.injectPref()
    private let handler: ServiceHandler<ChooseLunchApis> = ServiceHandler()
    
    func orderLunchMeal(type: ChooseLunchApis.OrderType) -> Observable<(Bool, String)> {
        guard
            let cn = pref.aliasName,
            let en = pref.aliasNameEn,
            let dep = pref.aliasDepartment,
            let site = pref.aliasSiteFloor
            else { return .empty() }
        
        return handler[.order(selection: type, cnName: cn, enName: en, depName: dep, site: site)]
            .map{ r in (r.isSuccess, type.rawValue) }
    }
}
