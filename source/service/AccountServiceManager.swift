//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class AccountServiceManager{
    private let handler: ServiceHandler<AccountApis> = ServiceHandler()
    
    private let pref = Injector.injectPref()

    func fetchAlias(account: String, password: String) -> Observable<(Bool, String)> {
        return handler[ .login(account: account, password: password) ]
            .do(onNext:{ [weak self] r in
                self?.accountSetting(data: r.jsonData, email: account)
            })
            .map{ ($0.isSuccess, $0.message) }
    }
    
    private func accountSetting(data: Any?, email: String) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard
            let dict = data as? [String: Any],
            let info = dict["info"] as? [String: Any],
            let dm = try? decoder.decode(AccountDm.self, from: info)
            else { return }
        
        pref.aliasToken = dm.token
        pref.aliasAccount = dm.empNo
        pref.aliasNum = dm.empNo
        pref.aliasName = dm.empName
        pref.aliasJobTitle = dm.jobEname
        pref.aliasDepartment = dm.depCname
        pref.aliasTel = dm.empTel
        pref.aliasSex = dm.empSex
        pref.aliasSiteFloor = dm.site
        pref.aliasPhoto = dm.photo
        pref.aliasOnBoardDay = dm.dayOnboard
        pref.aliasNameEn = dm.enFullName
        pref.aliasDepartmentEn = dm.depEname
        pref.aliasEmail = email // for account alias icon
        
        if pref.aliasUniqueId?.isEmpty == nil || pref.aliasUniqueId?.isEmpty == true {
            pref.aliasUniqueId = UIDevice.current.identifierForVendor?.uuidString // also save to keychain
        }
    }
}
