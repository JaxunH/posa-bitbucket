//
//  RequestParams.swift
//  posa
//
//  Created by JaxunC on 2018/9/11.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation
import Alamofire
typealias HTTPMethod = Alamofire.HTTPMethod

//
protocol RequestParams{
    var method: HTTPMethod { get }
    var params: [String: Any]?  { get }
    var path: String { get }
}
