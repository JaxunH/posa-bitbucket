//
//  ChooseLunchApis.swift
//  posa
//
//  Created by JaxunC on 2018/9/11.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

enum ChooseLunchApis{
    enum OrderType: String{
        case meat = "Regular"
        case vegetarian = "Vegetarian"
        case bySelf = "Self-prepared"
    }
    
    case order(selection: OrderType, cnName: String, enName: String, depName: String, site: String)
}
extension ChooseLunchApis: RequestParams{
    var method: HTTPMethod {
        switch self {
        case .order:
            return .post
        }
    }
    
    var params: [String : Any]? {
        switch self {
        case let .order(data):
            return [
                "cname": data.cnName,
                "ename": data.enName,
                "selection": data.selection.rawValue,
                "dep_name": data.depName,
                "site": data.site
            ]
        }
    }
    
    var path: String {
        switch self {
        case .order:
            return "MealOrder"
        }
    }
}
