//
//  PunchTimeApis.swift
//  posa
//
//  Created by JaxunC on 2018/9/13.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

enum PunchTimeApis{
    case clock(accountId: String, deviceId: String, punchType: Int) //0: on, 1: off
    case getTodayCard(accountId: String, today: String)
    case getWifiList
    case getBadgeCount(accountId: String)
    case getBPMInfo(accountId: String, mobile: String, code: String)
    case token(accountId:String, tokenId: String)
    case updateInfo(os:Int, versionNum: String)
}

extension PunchTimeApis: RequestParams{
    var method: HTTPMethod {
        switch self{
        case .getTodayCard, .getWifiList, .getBadgeCount, .getBPMInfo, .updateInfo:
            return .get
        case .clock, .token:
            return .post
        }
    }
    
    var params: [String : Any]? {
        switch self{
        case let .clock(data):
            var dict = ["status": data.punchType] as [String : Any]
            #if targetEnvironment(simulator)
            dict["device_id"] = "5FE4F167-B635-4287-B043-CF32A2A8F776"
            #else
            dict["device_id"] = data.deviceId
            #endif
            return dict
            
        case let .getTodayCard(data):
            return ["date": data.today]

        case .getWifiList:
            return nil
            
        case let .getBadgeCount(data):
            return nil

        case let .getBPMInfo(data):
            return ["mobile": data.mobile, "code": data.code, "token": data.code.reversed()]
            
        case let .token(data):
            return ["token": data.tokenId]
        case let .updateInfo(data):
            return ["os": data.os, "versionNumber": data.versionNum] //todo: rename
        }
    }
    
    var path: String {
        switch self{
        case .clock:
            return "clock"
        case .getTodayCard:
            return "getCardDataByDay"
        case .getWifiList:
            return "wifi"
        case .getBadgeCount:
            return "getFormBadgeCount"
        case .getBPMInfo:
            return "getBPMInfo"
        case .token:
            return "setTokensFromMobile"
        case .updateInfo:
            return "getAppVersionInfo"
        }
    }
}
