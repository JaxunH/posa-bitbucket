//
// Created by JaxunC on 2018/9/11.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

enum CalenderApis {
    typealias Params = (year: String, month: String)
    case fetchCardDataByMonth(Params)
    case fetchCardDataMatchByMonth(Params)
    case fetchMealOrderByMonth(Params)
}

extension CalenderApis: RequestParams {
    var method: HTTPMethod {
        switch self {
        case .fetchCardDataByMonth, .fetchCardDataMatchByMonth, .fetchMealOrderByMonth:
            return .get
        }
    }
    var params: [String: Any]? {
        var dict: Params!
        switch self {
        case let .fetchCardDataByMonth(data):
            dict = data
        case let .fetchCardDataMatchByMonth(data):
            dict = data
        case let .fetchMealOrderByMonth(data):
            dict = data
        }
        return [
            "year": dict.year,
            "month": dict.month
        ]
    }

    var path: String {
        switch self {
        case .fetchCardDataByMonth:
            return "getCardDataByMonth"
        case .fetchCardDataMatchByMonth:
            return "getCardDataMatchByMonth"
        case .fetchMealOrderByMonth:
            return "getMealOrderByMonth"
        }
    }
}
