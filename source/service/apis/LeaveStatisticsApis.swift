//
// Created by JaxunC on 2018-09-13.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

enum LeaveStatisticApis{
    case fetchAll(account: String, year: String)
    case fetchCompensatory(account: String, year: String)
    case fetchMaternity(account: String, year: String)
    case fetchBereavement(account: String, year: String)
    case fetchMenstrual(account: String, year: String)
    case fetchMarriage(account: String, year: String)
    case fetchPrenatalVisit(account: String, year: String)
    case fetchPaternity(account: String, year: String)
}

extension LeaveStatisticApis: RequestParams {
    var method: HTTPMethod {
        switch self {
        case .fetchAll,
             .fetchBereavement,
             .fetchCompensatory,
             .fetchMarriage,
             .fetchMaternity,
             .fetchMenstrual,
             .fetchPrenatalVisit,
             .fetchPaternity:
            return .get
        }

    }
    var params: [String: Any]? {
        switch self {
        case let .fetchAll(data),
             let .fetchPrenatalVisit(data),
             let .fetchBereavement(data),
             let .fetchCompensatory(data),
             let .fetchMarriage(data),
             let .fetchMaternity(data),
             let .fetchMenstrual(data),
             let .fetchPaternity(data):
            return ["year": data.year]
        }
    }
    var path: String {
        switch self {
        case .fetchAll:
            return "getEmployeeAllLeave"
        case .fetchBereavement:
            return "getBereavementLeave"
        case .fetchCompensatory:
            return "getCompensatoryLeave"
        case .fetchMarriage:
            return "getMarriageLeave"
        case .fetchMaternity:
            return "getMaternityLeave"
        case .fetchMenstrual:
            return "getMenstrualLeave"
        case .fetchPrenatalVisit:
            return "getPrenatalVisitLeave"
        case .fetchPaternity:
            return "getPaternityLeave"
        }
    }
}
