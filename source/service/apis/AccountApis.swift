//
//  AccountApis.swift
//  posa
//
//  Created by JaxunC on 2018/9/12.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Foundation

enum AccountApis {
    case login(account: String, password: String)
}

extension AccountApis: RequestParams {
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }

    var params: [String: Any]? {
        switch self {
        case let .login(data):
            return ["account": data.account, "password": data.password]
        }
    }

    var path: String {
        switch self {
        case .login:
            return "login"
        }
    }
}
