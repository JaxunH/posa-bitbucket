//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift


class LeaveStatisticsServiceManager {

    private let pref = Injector.injectPref()
    
    private var accountId: String { return pref.aliasAccount ?? "" }
    
    private let handler: ServiceHandler<LeaveStatisticApis> = ServiceHandler()

    func fetchAll(year: String) -> Observable<[EmployeeAllLeaveDm]> {
        return handler[.fetchAll(account: accountId, year: year)]
                .map(ServiceHandler.Result.toArray(EmployeeAllLeaveDm.self))
    }

    // 補休
    func fetchCompensatory(year: String) -> Observable<[CompensatoryLeaveDm]> {
        return handler[.fetchCompensatory(account: accountId, year: year)]
                .map(ServiceHandler.Result.toArray(CompensatoryLeaveDm.self))
    }
    
    // 喪假
    func fetchBereavement(year: String) -> Observable<[TypeOfLeaveDm]> {
        return handler[.fetchBereavement(account: accountId, year: year)]
                .map(ServiceHandler.Result.toArray(TypeOfLeaveDm.self))
    }

    //
    func fetchMenstrual(year: String) -> Observable<[MenstrualLeaveDm]> {
        return handler[.fetchMenstrual(account: accountId, year: year)]
        .map(ServiceHandler.Result.toArray(MenstrualLeaveDm.self))
    }

    func fetchMarriage(year: String) -> Observable<[DateOfEventLeaveDm]> {
        return handler[.fetchMarriage(account: accountId, year: year)]
        .map(ServiceHandler.Result.toArray(DateOfEventLeaveDm.self))
    }

    // The state of being a mother <產假>
    func fetchMaternity(year: String) -> Observable<[TypeOfLeaveDm]> {
        return handler[.fetchMaternity(account: accountId, year: year)]
            .map(ServiceHandler.Result.toArray(TypeOfLeaveDm.self))
    }
    
    // The fact of being a father <陪產假> <學英文好嗎>
    func fetchPaternity(year: String) -> Observable<[DateOfEventLeaveDm]> {
        return handler[.fetchPaternity(account: accountId, year: year)]
                .map(ServiceHandler.Result.toArray(DateOfEventLeaveDm.self))
    }

    func fetchPrenatalVisit(year: String) -> Observable<[DateOfEventLeaveDm]> {
        return handler[.fetchPrenatalVisit(account: accountId, year: year)]
                .map(ServiceHandler.Result.toArray(DateOfEventLeaveDm.self))
    }
}
