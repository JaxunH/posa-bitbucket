//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

private class Container {
    
    static let shared = Container()
    
    let pref = Pref()
    
    lazy var punchTimeServiceManager: PunchTimeServiceManager = PunchTimeServiceManager()
    lazy var leaveStatisticsServiceManager:LeaveStatisticsServiceManager = LeaveStatisticsServiceManager()
    lazy var mainCalenderServiceManager: MainCalenderServiceManager = MainCalenderServiceManager()
    lazy var accountServiceManager: AccountServiceManager = AccountServiceManager()
    lazy var choseLunchServiceManager: ChoseLunchServiceManager = ChoseLunchServiceManager()
    
    //VMPs
    var chooseLunchVMP: ChooseLunchVMP { return ChooseLunchViewModel() }
    var loginVMP: LoginVMP { return LoginViewModel() }
        
}

class Injector {
    
    private static let container = Container.shared
    
    public static func inject<T>() -> T {
        if T.self == PunchTimeServiceManager.self {
            return container.punchTimeServiceManager as! T
        } else if T.self == LeaveStatisticsServiceManager.self {
            return container.leaveStatisticsServiceManager as! T
        }
        
        else if T.self == AccountServiceManager.self {
            return container.accountServiceManager as! T
        } else if T.self == MainCalenderServiceManager.self {
            return container.mainCalenderServiceManager as! T
        } else if T.self == ChoseLunchServiceManager.self {
            return container.choseLunchServiceManager as! T
        }
        
        else if T.self == ChooseLunchVMP.self {
            return container.chooseLunchVMP as! T
        }else if T.self == LoginVMP.self {
            return container.loginVMP as! T
        }
        
        fatalError("no instance registered as type: \(T.self)")
    }
    
    static func injectPref() -> Pref {
        return container.pref
    }
}

