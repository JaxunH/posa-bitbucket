fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios l_archive
```
fastlane ios l_archive
```
bundle exec fastlane archive to:$IOS_TARGET version:$IOS_VERSION build:$IOS_BUILD
### ios l_slack
```
fastlane ios l_slack
```
bundle exec fastlane slack message:$SLACK_MSG
### ios l_test
```
fastlane ios l_test
```
bundle exec fastlane test scheme:$TEST_SCHEME

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
