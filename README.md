# Posa-iOS

104打卡系統 ＆ 新人類表單(請假+追蹤+簽核) ，往後預計陸續會新增更多表單...
目前以"Starlux"命名。

## Prerequisites

專案使用CocoaPods(請先行安裝)，點擊posa.xcworkspace開啟專案，
開啟Terminal在專案底下輸入：
```
pod init
```

## Deployment
目前自動部署使用fastlane結合Fabric，配合企業帳號發佈給公司同仁指令如下
```
fastlane fabric_enterprise
```
相關操作以及設定請參考專案/fastlane/Fatfile

## API
可先行下載postman。點擊下面連結開啟文件，文件右上方可點選Run in Postman匯入
UAT - https://documenter.getpostman.com/view/4409319/RWToPxWN
Release - 待補

## GitlabCI + fastlane
Before you use CI/CD:

For file: ".gitlab-ci.yml"
```
Set global variables "TEST_SCHEME"
Set tags on method ".common"
```

For file: "Fastfile"
```
Set all of global variables.
Finish deploy code on l_archive lane.
```

If you wanna set CI/CD with schedule,
```
please fill with key: "IOS_TARGET", value: "daily".
```
Otherwise using manual.
```
ref=[Branch Name]
IOS_BUILD=[Build Number]
IOS_TARGET=[Select one: develop, alpha, master]
```
Example:
```
curl -X POST \
     -F token=717ee47090a8917ecc74672fc32c95 \
     -F ref=refine/ci_fastlane \
     -F "variables[IOS_BUILD]=0926.2" \
     -F "variables[IOS_TARGET]=alpha" \
     http://gitlab.starlux-airlines.com/api/v4/projects/52/trigger/pipeline
```
